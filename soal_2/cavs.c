#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>

// membuat folder
void folderBaru(const char *namaFolder) {
    if (mkdir(namaFolder, 0777) == -1) {
        perror("Error");
        exit(1);
    }
    printf("Folder '%s' berhasil dibuat.\n", namaFolder);
}

// mengunduh file dari URL ke path yang ditentukan
void unduhFile(const char *url, const char *outputPath) {
    pid_t pid = fork();

    if (pid == 0) {
        // proses anak
        execl("/usr/bin/wget", "wget", url, "-O", outputPath, NULL);
        perror("exec(wget) failed");
        exit(EXIT_FAILURE);
    } else if (pid > 0) {
        // proses induk
        int status;
        waitpid(pid, &status, 0);

        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            printf("Selesai Mengunduh.\n");
        }
    }
}

// menghapus file zip
void hapusFile(const char *filePath) {
    if (remove(filePath) == 0) {
        printf("File '%s' berhasil dihapus.\n", filePath);
    } else {
        perror("Gagal Menghapus File");
    }
}

// memeriksa apakah pemain termasuk dalam Cleveland Cavaliers
int adalahCavaliers(const char *namaPemain) {
    if (strstr(namaPemain, "Cavaliers") != NULL) {
        return 1;  // Cavaliers
    }
    return 0;  // non-Cavaliers
}

// menghapus pemain non-Cleveland Cavaliers dari direktori
void hapusPemainNonCavaliers(const char *direktori) {
    DIR *dir;
    struct dirent *entry;

    dir = opendir(direktori);
    if (dir == NULL) {
        perror("Error membuka direktori");
        exit(1);
    }

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            // abaikan direktori "." (direktori saat ini) dan ".." (direktori induk)
            continue;
        }

        if (entry->d_type == DT_REG) {
            // memproses file (bukan direktori)
            if (!adalahCavaliers(entry->d_name)) {
                // hapus pemain non-Cavaliers
                char fullPath[512];
                snprintf(fullPath, sizeof(fullPath), "%s/%s", direktori, entry->d_name);
                hapusFile(fullPath);
            }
        }
    }

    closedir(dir);
}

// menghitung jumlah pemain dalam masing-masing kategori
void hitungPemainPerKategori(const char *direktori) {
    int jumlahPG = 0, jumlahSG = 0, jumlahSF = 0, jumlahPF = 0, jumlahC = 0;

    DIR *dir;
    struct dirent *entry;

    dir = opendir(direktori);
    if (dir == NULL) {
        perror("Error membuka direktori");
        exit(1);
    }

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            // abaikan direktori "." (direktori saat ini) dan ".." (direktori induk)
            continue;
        }

        if (entry->d_type == DT_REG) {
            // memproses file
            char namaPemain[256];
            strcpy(namaPemain, entry->d_name);
            
            if (strstr(namaPemain, "PG") != NULL) {
                jumlahPG++;
            } else if (strstr(namaPemain, "SG") != NULL) {
                jumlahSG++;
            } else if (strstr(namaPemain, "SF") != NULL) {
                jumlahSF++;
            } else if (strstr(namaPemain, "PF") != NULL) {
                jumlahPF++;
            } else if (strstr(namaPemain, "C") != NULL) {
                jumlahC++;
            }
        }
    }

    closedir(dir);

    char fullPath[512];
    snprintf(fullPath, sizeof(fullPath), "%s/Formasi.txt", getenv("HOME"));

    // menyimpan hasil kategorisasi
    FILE *fileFormasi = fopen("Formasi.txt", "w");
    if (fileFormasi == NULL) {
        perror("Error membuka Formasi.txt");
        exit(1);
    }

    fprintf(fileFormasi, "PG: %d\n", jumlahPG);
    fprintf(fileFormasi, "SG: %d\n", jumlahSG);
    fprintf(fileFormasi, "SF: %d\n", jumlahSF);
    fprintf(fileFormasi, "PF: %d\n", jumlahPF);
    fprintf(fileFormasi, "C: %d\n", jumlahC);

    fclose(fileFormasi);
}

int main() {
    folderBaru("Players");
    unduhFile("https://drive.google.com/u/0/uc?id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK&export=download", "Players/database.zip");

    // menunggu pengunduhan selesai
    sleep(5);  

    // fork untuk menjalankan unzip di dalam proses anak
    pid_t unzip_pid = fork();

    if (unzip_pid == 0) {
        // proses anak untuk unzip
        execl("/usr/bin/unzip", "unzip", "Players/database", "-d", "Players", NULL);
        perror("exec(unzip) failed");
        exit(EXIT_FAILURE);
    } else if (unzip_pid > 0) {
        // proses induk
        int unzip_status;
        waitpid(unzip_pid, &unzip_status, 0);

        if (WIFEXITED(unzip_status) && WEXITSTATUS(unzip_status) == 0) {
            // menghapus file zip setelah selesai unzip
            hapusFile("Players/database.zip");
            printf("Unzip selesai.\n");

            // hapus pemain nonCleveland Cavaliers
            hapusPemainNonCavaliers("Players");

            // hitung jumlah pemain dari masing-masing kategori
            hitungPemainPerKategori("Players");
        } else {
            printf("Unzip gagal.\n");
        }
    } else {
        perror("Error Forking");
    }

    folderBaru("clutch");

const char *kyrie = "Players/Cavaliers-PG-Kyrie-Irving.png";
const char *james = "Players/Cavaliers-SF-LeBron-James.png";
const char *clutchFolder = "clutch";

pid_t move_pid = fork();
if (move_pid == 0) {
    execl("/bin/mv", "mv", kyrie, clutchFolder, NULL);
    perror("exec(mv) failed");
    exit(EXIT_FAILURE);
} else if (move_pid > 0) {
    // menunggu proses anak selesai
    int move_status;
    waitpid(move_pid, &move_status, 0);

    if (WIFEXITED(move_status) && WEXITSTATUS(move_status) == 0) {
        printf("Poster Kyrie Irving dipindahkan ke folder clutch.\n");
    } else {
        printf("Gagal memindahkan Poster Kyrie Irving ke folder clutch.\n");
    }
}

pid_t move_pid2 = fork();
if (move_pid2 == 0) {
    execl("/bin/mv", "mv", james, clutchFolder, NULL);
    perror("exec(mv) failed");
    exit(EXIT_FAILURE);
} else if (move_pid2 > 0) {
    // menunggu proses anak selesai
    int move_status2;
    waitpid(move_pid2, &move_status2, 0);

    if (WIFEXITED(move_status2) && WEXITSTATUS(move_status2) == 0) {
        printf("Poster LeBron James dipindahkan ke folder clutch.\n");
    } else {
        printf("Gagal memindahkan Poster LeBron James ke folder clutch.\n");
    }
}

    return 0;
}
