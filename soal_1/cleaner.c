#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>

#define MAX_PATH_LENGTH 1024
#define SEARCH_STRING "SUSPICIOUS"
#define LOG_FILE "cleaner.log"

void search_and_clean(const char *directory_path);

void write_log(const char *message);


int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Penggunaan: %s <direktori>\n", argv[0]);
        exit(1);
    }

    const char *directory_path = argv[1];
    search_and_clean(directory_path);
    return EXIT_SUCCESS;
}

void write_log(const char *message) {
    FILE *log_file = fopen(LOG_FILE, "a");

    if (log_file != NULL) {
        fprintf(log_file, "%s\n", message);
        fclose(log_file);
    }
}

void search_and_clean(const char *directory_path) {
    DIR *dir;
    struct dirent *entry;

    if ((dir = opendir(directory_path)) == NULL) {
        perror("Gagal membuka direktori");
        exit(EXIT_FAILURE);
    }

    char full_path[MAX_PATH_LENGTH];

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            snprintf(full_path, sizeof(full_path), "%s/%s", directory_path, entry->d_name);
            FILE *file = fopen(full_path, "r");

            if (file != NULL) {
                char line[MAX_PATH_LENGTH];
                while (fgets(line, sizeof(line), file) != NULL) {
                    if (strstr(line, SEARCH_STRING) != NULL) {
                        fclose(file);
                        remove(full_path);
                        char log_message[2048];
                        snprintf(log_message, sizeof(log_message), "File '%s' mengandung kata '%s' dan telah dihapus.", full_path, SEARCH_STRING);
                        write_log(log_message);
                        break;
                    }
                }
                fclose(file);
            }
        }
    }

    closedir(dir);
}
