## Sisop Modul 2

# Nomor 1

John adalah seorang mahasiswa biasa. Pada tahun ke-dua kuliahnya, dia merasa bahwa dia telah menyianyiakan waktu kuliahnya selama ini. Selama mengerjakan tugas, John selalu menggunakan bantuan ChatGPT dan tidak pernah mempelajari apapun dari hal tersebut. Untuk itu, pada soal kali ini John bertekad untuk tidak menggunakan ChatGPT dan mencoba menyelesaikan tugasnya dengan tangan dan pikirannya sendiri. Melihat tekad yang kuat dari John, Mark, dosen yang mengajar John, ingin membantunya belajar dengan memberikan sebuah ujian. Sebelum memberikan ujian pada John, Mark berpesan bahwa John harus bersungguh-sungguh dalam mengerjakan ujian, fokus untuk belajar, dan tidak perlu khawatir akan nilai yang diberikan. Mark memberikan ujian pada John untuk membuatkannya sebuah program `cleaner` sederhana dengan ketentuan berikut :
Program tersebut menerima input path dengan menggunakan “argv”.
Program tersebut bertugas untuk menghapus file yang didalamnya terdapat string "SUSPICIOUS" pada direktori yang telah diinputkan oleh user.
Program tersebut harus berjalan secara daemon.
Program tersebut akan terus berjalan di background dengan jeda 30 detik.
Dalam pembuatan program tersebut, tidak diperbolehkan menggunakan fungsi `system()`.
Setiap kali program tersebut menghapus sebuah file, maka akan dicatat pada file 'cleaner.log' yang ada pada direktori home user dengan format seperti berikut : “[YYYY-mm-dd HH:MM:SS] '<absolute_path_to_file>' has been removed.”.

Example usage:	
./cleaner '/home/user/cleaner'


Example cleaner.log
[2023-10-02 18:54:20] '/home/user/cleaner/is.txt' has been removed.
[2023-10-02 18:54:20] '/home/user/cleaner/that.txt' has been removed.
[2023-10-02 18:54:50] '/home/user/cleaner/who.txt' has been removed.


Example valid sample file:
LJuUHmAnVLLCMRhLTcqy
bBpSUSPICIOUSagQKmLA
BitSuQNHSLmZDvEcvbGc


Example invalid sample file:
LJuUHmAnVLLCMRhLTcqy
TFvefehhpWDCbkdirmlh
BitSuQNHSLmZDvEcvbGc

**Pengerjaan Nomor 1:**

    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <fcntl.h>
    #include <unistd.h>
    #include <dirent.h>

    #define MAX_PATH_LENGTH 1024
    #define SEARCH_STRING "SUSPICIOUS"
    #define LOG_FILE "cleaner.log"

    void search_and_clean(const char *directory_path);

    void write_log(const char *message);


    int main(int argc, char *argv[]) {
        if (argc != 2) {
            fprintf(stderr, "Penggunaan: %s <direktori>\n", argv[0]);
            exit(1);
        }

        const char *directory_path = argv[1];
        search_and_clean(directory_path);
        return EXIT_SUCCESS;
    }

    void write_log(const char *message) {
        FILE *log_file = fopen(LOG_FILE, "a");

        if (log_file != NULL) {
            fprintf(log_file, "%s\n", message);
            fclose(log_file);
        }
    }

    void search_and_clean(const char *directory_path) {
        DIR *dir;
        struct dirent *entry;

        if ((dir = opendir(directory_path)) == NULL) {
            perror("Gagal membuka direktori");
            exit(EXIT_FAILURE);
        }

        char full_path[MAX_PATH_LENGTH];

        while ((entry = readdir(dir)) != NULL) {
            if (entry->d_type == DT_REG) {
                snprintf(full_path, sizeof(full_path), "%s/%s", directory_path, entry->d_name);
                FILE *file = fopen(full_path, "r");

                if (file != NULL) {
                    char line[MAX_PATH_LENGTH];
                    while (fgets(line, sizeof(line), file) != NULL) {
                        if (strstr(line, SEARCH_STRING) != NULL) {
                            fclose(file);
                            remove(full_path);
                            char log_message[2048];
                            snprintf(log_message, sizeof(log_message), "File '%s' mengandung kata '%s' dan telah dihapus.", full_path, SEARCH_STRING);
                            write_log(log_message);
                            break;
                        }
                    }
                    fclose(file);
                }
            }
        }

        closedir(dir);
    }


Header-header tersebut memberi izin akses ke fungsi-fungsi dan struktur data yang diperlukan oleh program. Konstanta MAX_PATH_LENGTH dipakai untuk menentukan panjang maksimum jalur file, SEARCH_STRING berisi string yang akan dicari di dalam file, dan LOG_FILE yang adalah nama file log. Fungsi write_log berguna untuk menulis pesan ke dalam file log. Program membuka file log dengan mode "append" ("a") dan memasukkan pesan yang diberikan. Program mencari file yang mengandung string "SUSPICIOUS" menggunakan fungsi search_and_clean dalam direktori yang ditentukan lalu mengiterasi melalui semua file di dalam direktori, kemudian membuka setiap file untuk membaca baris-barisnya. Jika berhasil menemukan baris yang mengandung "SUSPICIOUS," program akan menghapus file tersebut dan mencatatnya ke dalam file log. Dalam program ini terdapat loop non-stop di dalam fungsi main. Setelah selesai menelusuri direktori, program akan tertidur selama 30 detik, lalu melanjutkan pekerjaannya tadi.


**Output Nomor 1:**

![output_1](gambar/soal_1/1.jpeg)



# Nomor 2

QQ adalah fan Cleveland Cavaliers. Ia ingin memajang kamarnya dengan poster foto roster Cleveland Cavaliers tahun 2016. Maka yang dia lakukan adalah meminta tolong temannya yang sangat sisopholic untuk membuatkannya sebuah program untuk mendownload gambar - gambar pemain tersebut. Sebagai teman baiknya, bantu QQ untuk mencarikan foto foto yang dibutuhkan QQ dengan ketentuan sebagai berikut:
Pertama, buatlah program bernama “cavs.c” yang dimana program tersebut akan membuat folder “players”.
Program akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip” ke dalam folder players yang telah dibuat. Lalu hapus file zip tersebut agar tidak memenuhi komputer QQ.
Dikarenakan database yang diunduh masih data mentah. Maka bantulah QQ untuk menghapus semua pemain yang bukan dari Cleveland Cavaliers yang ada di directory.
Setelah mengetahui nama-nama pemain Cleveland Cavaliers, QQ perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 5 proses yang berbeda. Untuk kategori folder akan menjadi 5 yaitu point guard (PG), shooting guard (SG), small forward (SF), power forward (PF), dan center (C).
Hasil kategorisasi akan di outputkan ke file Formasi.txt, dengan berisi
PG: {jumlah pemain}
SG: {jumlah pemain}
SF: {jumlah pemain}
PF: {jumlah pemain}
C: {jumlah pemain}
Ia ingin memajang foto pemain yang menembakkan game-winning shot pada ajang NBA Finals 2016, tepatnya pada game 7, dengan membuat folder “clutch”, yang di dalamnya berisi foto pemain yang bersangkutan.
Ia merasa kurang lengkap jika tidak memajang foto pemain yang melakukan The Block pada ajang yang sama, Maka dari itu ditaruhlah foto pemain tersebut di folder “clutch” yang sama.
	Catatan:
Format nama file yang akan diunduh dalam zip berupa [tim]-[posisi]-[nama].png
Tidak boleh menggunakan system(), Gunakan exec() dan fork().
Directory “.” dan “..” tidak termasuk yang akan dihapus.
2 poin soal terakhir dilakukan setelah proses kategorisasi selesai

**Pengerjaan Soal Nomor 2:**

    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <string.h>
    #include <sys/stat.h>
    #include <sys/types.h>
    #include <sys/wait.h>
    #include <dirent.h>

library-library tersebut digunakan antara lain untuk operasi input/output (stdio.h), fungsi manajemen memori (stdlib.h), mengakses fungsi sistem dan sistem panggilan pada OS Unix-like (unistd.h), memanipulasi string (string.h), mengakses dan memanipulasi informasi status berkas (sys.sat.h), mendefinisikan tipe data dasar yang digunakan dalam OS (sys types.h), mengontrol proses dan menunggu child process selesai (sys/wait.h), dan berinteraksi dengan direktori dan berkas dalam sistem berkas (dirent.h).

    // membuat folder
    void folderBaru(const char *namaFolder) {
        if (mkdir(namaFolder, 0777) == -1) {
        perror("Error");
        exit(1);
        }
    printf("Folder '%s' berhasil dibuat.\n", namaFolder);
    }

merupakan deklarasi fungsi void bernama folderBaru yang menerima argumen namaFolder untuk membuat folder di dalam program dengan menggunakan perintah mkdir. Jika mengembalikan nilai -1, maka pembuatan direktori atau folder baru gagal dilakukan dan menampilkan output "Error" dengan fungsi perror. Jika folder baru berhasil dibuat, maka akan menampilkan output "Folder (namaFolder) berhasil dibuat."

    // mengunduh file dari URL ke path yang ditentukan
    void unduhFile(const char *url, const char *outputPath) {
        pid_t pid = fork();
        
        if (pid == 0) {
        // child process
        execl("/usr/bin/wget", "wget", url, "-O", outputPath, NULL);
        perror("exec(wget) failed");
        exit(EXIT_FAILURE);
        }
        
        else if (pid > 0) {
        // parent process
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            printf("Selesai Mengunduh.\n");
            }
        }
    }

merupakan deklarasi fungsi void bernama unduhFile yang menerima argumen url sebagai url dari file yang akan didownload dan argumen outputPath sebagai jalur penyimpanan file yang didownload. Dengan menggunakan fork(), bagian ini akan menghasilkan proses baru yang merupakan salinan dari parent process dengan process ID (PID) yang berbeda. Nilai PID adalah 0 jika kode yang dijalankan ada di child process, dan akan menjadi child PID baru jika kodenya ada di parent process. Child process akan menjalankan wget menggunakan execl. Argumen /usr/bin/wget adalah path menuju program wget, lalu wget sendiri adalah nama programnya, diikuti url yang akan didownload, opsi -O untuk menentukan output file, serta null untuk penanda akhir argumen. 

Jika exec gagal dijalankan, maka kode error akan dicetak dengan perror dan child process akan keluar bersama kode error memakai exit(EXIT_FAILURE). Parent process akan menunggu hingga child process selesai menggunakan waitpid. Program bisa mengetahui apakah child process sukses memakai makro WIFEXITED dan WEXITSTATUS. Jika child process selesai dengan kode 0, berarti pengunduhan selesai tanpa error dan akan menampilkan output "Selesai Mengunduh.".


    // menghapus file zip
    void hapusFile(const char *filePath) {
        if (remove(filePath) == 0) {
            printf("File '%s' berhasil dihapus.\n", filePath);
        } else {
            perror("Gagal Menghapus File");
        }
    }

merupakan deklarasi fungsi void bernama hapusFile yang menerima argumen filePath sebagai jalur ke file yang ingin dihapus. Fungsi remove dipanggil dalam if untuk menghapus file. Jika berhasil maka akan mengembalikan nilai 0 dan menampilkan output "File (filePath) berhasil dihapus.". Jika nilai yang dikembalikan remove bukan 0, berarti penghapusan gagal dilakukan sehingga fungsi perror akan menampilkan output "Gagal Menghapus File".



    // memeriksa apakah pemain termasuk dalam Cleveland Cavaliers
    int adalahCavaliers(const char *namaPemain) {
        if (strstr(namaPemain, "Cavaliers") != NULL) {
            return 1;  // Cavaliers
        }
        return 0;  // non-Cavaliers
    }

merupakan deklarasi fungsi integer bernama adalahCavaliers yang menerima argumen namaPemain. Fungsi strstr (string search) dipakai untuk mencari string "Cavaliers" di dalam namaPemain. Jika strstr menemukan substring "Cavaliers" dalam namaPemain, maka pointer akan dikembalikan dari substring tersebut. Jika tidak ditemukan, maka strstr akan mengembalikan NULL. Jika substring "Cavaliers" ditemukan, maka program akan mengembalikan nilai 1, yang berarti pemain tersebut dari tim "Cavaliers.". Jika substring "Cavaliers" tidak ditemukan, maka program akan mengembalikan nilai 0, yang berarti pemain tersebut bukan dari tim "Cavaliers."


    // menghapus pemain non-Cleveland Cavaliers dari direktori
    void hapusPemainNonCavaliers(const char *direktori) {
        DIR *dir;
        struct dirent *entry;

        dir = opendir(direktori);
        if (dir == NULL) {
            perror("Error membuka direktori");
            exit(1);
        }

        while ((entry = readdir(dir)) != NULL) {
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
                // abaikan direktori "." (direktori saat ini) dan ".." (direktori induk)
                continue;
            }

            if (entry->d_type == DT_REG) {
                // memproses file (bukan direktori)
                if (!adalahCavaliers(entry->d_name)) {
                    // hapus pemain non-Cavaliers
                    char fullPath[512];
                    snprintf(fullPath, sizeof(fullPath), "%s/%s", direktori, entry->d_name);
                    hapusFile(fullPath);
                }
            }
        }

        closedir(dir);
    }

merupakan deklarasi fungsi void bernama hapusPemainNonCavaliers yang menerima argumen direktori yang merupakan jalur ke direktori yang filenya akan dihapus kecuali pemain Cavaliers. Variabel dir dan entry dideklarasikan untuk mewakili direktori yang sedang dibuka dan file/folder yang ada di dalamnya. Direktori akan dibuka melalui fungsi opendir dan mengembalikan pointer ke struktur DIR, jika tidak terbuka maka akan menghasilkan NULL dan perror akan menampilkan "Error membuka direktori" serta keluar dengan exit(1). Terdapat loop untuk mengiterasi melalui semua file/folder dalam direktori yang telah dibuka. Melalui fungsi readdir, program akan membaca entri berikutnya dalam direktori. Jika tidak tersedia, maka loop berhenti. Saat loop berjalan, program memeriksa apakah entri saat ini adalah direktori "." (direktori saat ini) atau ".." (direktori induk), keduanya diabaikan. Setelah dipastikan bahwa entri bukan di direktori "." atau "..", program memeriksa apakah entri tersebut adalah sebuah file (bukan direktori) menggunakan d_type dari struktur dirent. 

Dalam kondisi ini, program memanggil fungsi adalahCavaliers untuk memeriksa apakah pemain yang terkait dengan nama file (entry->d_name) adalah dari tim "Cavaliers" atau tidak. Jika bukan, program membangun path lengkap untuk file yang akan dihapus dengan menggabungkan direktori dengan nama file yang ditemukan (entry->d_name) menggunakan snprintf. Kemudian, setelah path lengkap sukses dibangun, fungsi hapusFile akan dipanggil oleh program untuk menghapus file tersebut. Terakhir, program akan menutup direktori memakai closedir.



    // menghitung jumlah pemain dalam masing-masing kategori
    void hitungPemainPerKategori(const char *direktori) {
        int jumlahPG = 0, jumlahSG = 0, jumlahSF = 0, jumlahPF = 0, jumlahC = 0;

        DIR *dir;
        struct dirent *entry;

        dir = opendir(direktori);
        if (dir == NULL) {
            perror("Error membuka direktori");
            exit(1);
        }

        while ((entry = readdir(dir)) != NULL) {
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
                // abaikan direktori "." (direktori saat ini) dan ".." (direktori induk)
                continue;
            }

            if (entry->d_type == DT_REG) {
                // memproses file
                char namaPemain[256];
                strcpy(namaPemain, entry->d_name);
                
                if (strstr(namaPemain, "PG") != NULL) {
                    jumlahPG++;
                } else if (strstr(namaPemain, "SG") != NULL) {
                    jumlahSG++;
                } else if (strstr(namaPemain, "SF") != NULL) {
                    jumlahSF++;
                } else if (strstr(namaPemain, "PF") != NULL) {
                    jumlahPF++;
                } else if (strstr(namaPemain, "C") != NULL) {
                    jumlahC++;
                }
            }
        }

        closedir(dir);

        char fullPath[512];
        snprintf(fullPath, sizeof(fullPath), "%s/Formasi.txt", getenv("HOME"));

        // menyimpan hasil kategorisasi
        FILE *fileFormasi = fopen("Formasi.txt", "w");
        if (fileFormasi == NULL) {
            perror("Error membuka Formasi.txt");
            exit(1);
        }

        fprintf(fileFormasi, "PG: %d\n", jumlahPG);
        fprintf(fileFormasi, "SG: %d\n", jumlahSG);
        fprintf(fileFormasi, "SF: %d\n", jumlahSF);
        fprintf(fileFormasi, "PF: %d\n", jumlahPF);
        fprintf(fileFormasi, "C: %d\n", jumlahC);

        fclose(fileFormasi);
    }

merupakan deklarasi fungsi void bernama hitungPemainPerKategori yang menerima argumen direktori, yang merupakan jalur ke direktori yang akan dibaca. Variabel jumlahPG, jumlahSG, jumlahSF, jumlahPF, dan jumlahC dipakai untuk menyimpan jumlah pemain dalam masing-masing posisi (point guard, shooting guard, small forward, power forward, dan center). Variabel dir digunakan untuk mewakili direktori yang sedang dibuka, dan entry dipakai untuk mewakili file/folder dalam direktori tersebut. Fungsi opendir digunakan untuk membuka direktori dan mengembalikan pointer ke struktur DIR. Jika terbuka, maka akan menghasilkan NULL. if (dir == NULL) dijalankan ketika opendir gagal membuka direktori dan perror akan mencetak kode error terkait dengan kesalahan membuka direktori, dan program keluar dengan (exit(1)).

Loop digunakan untuk mengiterasi melalui semua file/folder dalam direktori terbuka. Fungsi readdir digunakan untuk membaca entri berikutnya dalam direktori. Jika tidak ada, maka loop terhenti. Di dalam loop, program membaca apakah entri saat ini adalah direktori "." (direktori saat ini) atau ".." (direktori induk). Keduanya diabaikan. Setelah dipastikan jika entrinya bukan direktori "." atau "..", program memeriksa apakah entri tersebut adalah sebuah file (bukan direktori) menggunakan d_type dari struktur dirent.

Program akan menyalin nama pemain (file) ke dalam variabel namaPemain agar diproses. Selanjutnya, dengan strstr, program akan mencari kategori pemain dalam nama file. Jika pemain termasuk dalam salah satu kategori (misalnya, "PG" untuk point guard), maka jumlah pemain dalam kategori tersebut akan ditambah sesuai dengan aturan yang sesuai. Jika sudah selesai, program akan menutup direktori dengan menggunakan closedir. Kemudian program menginisialisasi variabel fullPath untuk menyimpan jalur lengkap ke file "Formasi.txt" dalam direktori home pengguna. Setelahnya, program akan membuka file "Formasi.txt" untuk diisi dengan hasil perhitungan dengan menggunakan fopen. Jika gagal, program akan menghasilkan kode error dan keluar dengan kode tersebut. fprintf digunakan untuk menulis jumlah pemain dalam masing-masing kategori ke dalam file "Formasi.txt." Terakhir, file "Formasi.txt" ditutup dengan memakai fclose.



    int main() {
        folderBaru("Players");
        unduhFile("https://drive.google.com/u/0/uc?id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK&export=download", "Players/database.zip");

        // menunggu pengunduhan selesai
        sleep(5);  

        // fork untuk menjalankan unzip di dalam proses anak
        pid_t unzip_pid = fork();

        if (unzip_pid == 0) {
            // proses anak untuk unzip
            execl("/usr/bin/unzip", "unzip", "Players/database", "-d", "Players", NULL);
            perror("exec(unzip) failed");
            exit(EXIT_FAILURE);
        } else if (unzip_pid > 0) {
            // proses induk
            int unzip_status;
            waitpid(unzip_pid, &unzip_status, 0);

            if (WIFEXITED(unzip_status) && WEXITSTATUS(unzip_status) == 0) {
                // menghapus file zip setelah selesai unzip
                hapusFile("Players/database.zip");
                printf("Unzip selesai.\n");

                // hapus pemain nonCleveland Cavaliers
                hapusPemainNonCavaliers("Players");

                // hitung jumlah pemain dari masing-masing kategori
                hitungPemainPerKategori("Players");
            } else {
                printf("Unzip gagal.\n");
            }
        } else {
            perror("Error Forking");
        }

        folderBaru("clutch");

    const char *kyrie = "Players/Cavaliers-PG-Kyrie-Irving.png";
    const char *james = "Players/Cavaliers-SF-LeBron-James.png";
    const char *clutchFolder = "clutch";

    pid_t move_pid = fork();
    if (move_pid == 0) {
        execl("/bin/mv", "mv", kyrie, clutchFolder, NULL);
        perror("exec(mv) failed");
        exit(EXIT_FAILURE);
    } else if (move_pid > 0) {
        // menunggu proses anak selesai
        int move_status;
        waitpid(move_pid, &move_status, 0);

        if (WIFEXITED(move_status) && WEXITSTATUS(move_status) == 0) {
            printf("Poster Kyrie Irving dipindahkan ke folder clutch.\n");
        } else {
            printf("Gagal memindahkan Poster Kyrie Irving ke folder clutch.\n");
        }
    }

    pid_t move_pid2 = fork();
    if (move_pid2 == 0) {
        execl("/bin/mv", "mv", james, clutchFolder, NULL);
        perror("exec(mv) failed");
        exit(EXIT_FAILURE);
    } else if (move_pid2 > 0) {
        // menunggu proses anak selesai
        int move_status2;
        waitpid(move_pid2, &move_status2, 0);

        if (WIFEXITED(move_status2) && WEXITSTATUS(move_status2) == 0) {
            printf("Poster LeBron James dipindahkan ke folder clutch.\n");
        } else {
            printf("Gagal memindahkan Poster LeBron James ke folder clutch.\n");
        }
    }

        return 0;
    }

Di dalam fungsi utama, direktori baru bernama "Players" dibuat menggunakan fungsi folderBaru. Kemudian fungsi unduhFile mengunduh file dari URL yang ada dan disimpan dengan nama "database.zip" di dalam direktori "Players.". Program akan tidur selama 5 detik untuk memberi waktu sampai proses pengunduhan selesai. Fungsi fork digunakan untuk menciptakan child process yang akan digunakan untuk menjalankan perintah unzip pada file "database.zip". Fungsi execl digunakan untuk menjalankan perintah unzip pada file "database.zip" dan mengekstraknya ke dalam direktori "Players."

Fungsi waitpid digunakan untuk menunggu hingga child process selesai dan mengambil status keluaran dari proses anak.
Lalu diperiksa apakah proses unzip selesai dengan sukses. Jika berhasil, file "database.zip" yang telah di-ekstrak akan dihapus serta menghapus pemain non-Cavaliers dan menghitung pemain per kategori. Jika gagal, program mencetak kode error.

Setelah itu, direktori baru bernama "clutch" dibuat oleh program menggunakan fork untuk menciptakan child process dengan perintah mvs untuk memindahkan file pemain ke direktori "clutch". Lalu program akan memeriksa apakah pemindahan berhasil atau tidak.

**Output Nomor 2:**

![download_zip](gambar/soal_2/download_zip.jpeg)

Program mengunduh file zip

![extract](gambar/soal_2/extract.jpeg)

Program mengekstrak file zip

![hapus_nonCavaliers](gambar/soal_2/hapus_nonCavaliers.jpeg)

Program menghapus pemain yang bukan dari tim Cavaliers

![cek_direktori](gambar/soal_2/cek_direktori.jpeg)

Mengecek hasil kerja program, berhasil membuat fila dan folder yang harus dibuat

![isi_folder_Players](gambar/soal_2/isi_folder_Players.jpeg)

Isi dari folder Players

![isi_folder_clutch](gambar/soal_2/isi_folder_clutch.jpeg)

Isi dari folder clutch

![isi_file_Formasi](gambar/soal_2/isi_file_Formasi.jpeg)

Isi dari file Formasi


# Nomor 3

Albedo adalah seorang seniman terkenal dari Mondstadt. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini Albedo sedang menghadapi creativity block. Sebagai teman berkebangsaan dari Fontaine yang jago sisop, bantu Albedo untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !

 a. Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].

 b. Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://source.unsplash.com/{widthxheight} , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].

 c. Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip, format nama [YYYY-mm-dd_HH:mm:ss].zip tanpa “[]”).

 d. Karena takut program tersebut lepas kendali, Albedo ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.

 e. Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

# Catatan

• Tidak boleh menggunakan system()

• Proses berjalan secara daemon

• Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)

# Code Soal 3

    #include <signal.h>
    #include <sys/types.h>
    #include <sys/stat.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <fcntl.h>
    #include <errno.h>
    #include <unistd.h>
    #include <syslog.h>
    #include <string.h>
    #include <time.h>
    #include <wait.h>
    #include <sys/prctl.h>
    #include <stdbool.h>

    void makedaemon()
    {
    pid_t pid, sid;
    pid = fork();
    if (pid < 0) exit(EXIT_FAILURE);
    if (pid > 0) exit(EXIT_SUCCESS);
    umask(0);
    sid = setsid();
    if (sid < 0) exit(EXIT_FAILURE);
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    }

    int main(int argc, char* argv[])
    {
    if (argc != 2 || (argv[1][1] != 'a' && argv[1][1] != 'b')   ) 
    {
    printf("Pilih salah satu:\nmode -a : -a\nmode -b : -b\n");
        exit(0);
    }

    int stat;
    FILE* killer;
    killer = fopen("killer.sh", "w");
    
    if (argv[1][1] == 'a') {
        fprintf(killer, "#!/bin/bash\nkill %d\nkill %d\necho 'Program Dihentikan.'\nrm \"$0\"", getpid() + 2, getpid() + 3);
    } else if (argv[1][1] == 'b') {
        fprintf(killer, "#!/bin/bash\nkill %d\necho 'Program Dihentikan.'\nrm \"$0\"", getpid() + 2);
    }

    fclose(killer);

    pid_t cid;
    cid = fork();
    if (cid < 0) exit(0);
    if (cid == 0)
    {
        char *command[] = {"chmod", "u+x", "killer.sh", NULL};
        execv("/bin/chmod", command);
    }
    while (wait(&stat) > 0);

    makedaemon();

    char waktu[30], waktu2[30], waktu3[30], link[50];
    int stat1, stat2, stat3;

    while (1)
    {
        int i;
        pid_t cid, cid2, cid3, cid4, cid5, pid_before_fork = getpid() + 2;
        time_t t1 = time(NULL);
        struct tm* p1 = localtime(&t1);
        strftime(waktu, 30, "%Y-%m-%d_%H:%M:%S", p1);

        cid = fork();
        if (cid < 0) exit(0);
        if (cid == 0)
        {
            char *command[] = {"mkdir", waktu, NULL};
            execv("/bin/mkdir", command);
        }

        while (wait(&stat1) > 0);
        cid2 = fork();
        if (cid2 < 0) exit(0);
        if (cid2 == 0)
        {
            if (argv[1][1] == 'a') prctl(PR_SET_PDEATHSIG, SIGHUP);
            chdir(waktu);
            for (i = 0; i < 15; i++)
            {
                time_t n = time(NULL);
                struct tm* local = localtime(&n);
                strftime(waktu2, 30, "%Y-%m-%d_%H:%M:%S", local);
                sprintf(link, "https://picsum.photos/%ld", (n % 1000) + 50);

                cid3 = fork();
                if (cid3 < 0) exit(0);
                if (cid3 == 0)
                {
                    char *command[] = {"wget", link, "-O", waktu2, "-o", "/dev/null", NULL};
                    execv("/usr/bin/wget", command);
                }
                sleep(5);
            }

            while (wait(&stat2) > 0);

            chdir("..");
            strcpy(waktu3, waktu);
            strcat(waktu3, ".zip");

            cid4 = fork();
            if (cid4 < 0) exit(0);
            if (cid4 == 0)
            {
                char *command[] = {"zip", "-r", waktu3, waktu, NULL};
                execv("/usr/bin/zip", command);
            }

            while (wait(&stat3) > 0);
            sleep(1);
            cid5 = fork();
            if (cid5 < 0) exit(0);
            if (cid5 == 0)
            {
                char *command[] = {"rm", "-r", waktu, NULL};
                execv("/bin/rm", command);
            }
            if (getppid() != pid_before_fork) exit(1);
        }
        sleep(30);
    }
    }

# Penjelasan Soal 3

**Soal A : Membuat folder dengan timestamp per 30 detik**

    while (1)
    {
        int i;
        pid_t cid, cid2, cid3, cid4, cid5, pid_before_fork = getpid() + 2;
        time_t t1 = time(NULL);
        struct tm* p1 = localtime(&t1);
        strftime(waktu, 30, "%Y-%m-%d_%H:%M:%S", p1);

        cid = fork();
        if (cid < 0) exit(0);
        if (cid == 0)
        {
            char *command[] = {"mkdir", waktu, NULL};
            execv("/bin/mkdir", command);
        }
    // Kodingan Soal B&C
    sleep (30)

Pada kode diatas folder dibuat dengan mengeksekusi execv untuk menjalankan perintah mkdir, kemudian format nama time stamp diambil menggunakan `time(NULL)` dan kemudian mengonversinya ke dalam struktur `tm` dengan `localtime(&t1)`. Tanggal dan waktu ini kemudian diformat menjadi string dengan format "YYYY-MM-DD_HH:MM:SS" dan disimpan dalam variabel `waktu`. kemudian untuk memberikan jeda 30 detik, digunakan code `sleep (30)`.

**Soal B : Mendownload Gambar dari source.unplash.com setiap 5 detik, dengan ketentuan, dan memasukkannya kedalam folder**

        while (wait(&stat1) > 0);
        cid2 = fork();
        if (cid2 < 0) exit(0);
        if (cid2 == 0)
        {
            if (argv[1][1] == 'a') prctl(PR_SET_PDEATHSIG, SIGHUP);
            chdir(waktu);
            for (i = 0; i < 15; i++)
            {
                time_t n = time(NULL);
                struct tm* local = localtime(&n);
                strftime(waktu2, 30, "%Y-%m-%d_%H:%M:%S", local);
                sprintf(link, "https://picsum.photos/%ld", (n % 1000) + 50);

                cid3 = fork();
                if (cid3 < 0) exit(0);
                if (cid3 == 0)
                {
                    char *command[] = {"wget", link, "-O", waktu2, "-o", "/dev/null", NULL};
                    execv("/usr/bin/wget", command);
                }
                sleep(5);
            }

Pada kode diatas gambar didownload menggunakan `wget`, untuk mengatur supaya gambar yang terdownload sesuai ketentuan maka digunakanlah (n % 1000) + 50. Untuk memberikan jeda gambar selama 5 detik digunakan `sleep(5)`. kemudian code dapat membuat gambar masuk kedalam folder waktu karena menggunakan `chdir(waktu)`.

**Soal C : Menghapus folder apabila sudah terisi oleh 15 gambar, kemudian folder tersebut di zip dengan nama timestamp**

    for (i = 0; i < 15; i++)
    //Kodingan Soal B
    while (wait(&stat2) > 0);
    chdir("..");
    strcpy(waktu3, waktu);
    strcat(waktu3, ".zip");

    cid4 = fork();
    if (cid4 < 0) exit(0);
    if (cid4 == 0)
    {
        char *command[] = {"zip", "-r", waktu3, waktu, NULL};
        execv("/usr/bin/zip", command);
    }

    while (wait(&stat3) > 0);
    sleep(1);
    cid5 = fork();
    if (cid5 < 0) exit(0);
    if (cid5 == 0)
    {
        char *command[] = {"rm", "-r", waktu, NULL};
        execv("/bin/rm", command);
    }

Pada kode diatas supaya file file dapat berhenti mengisi file setelah mengunduh 15 gambar, maka digunakan kode `for (i = 0; i < 15; i++)` sehingga pada saat menyentuh angka 15, maka child akan berhenti. kemudian terdapat child yang bertanggung jawab untuk mengzip file yaitu dengan `char *command[] = {"zip", "-r", waktu3, waktu, NULL};` dan nama file didapatkan dari `waktu3`. setelah file menjadi zip, maka folder akan dihapus menggunakan `char *command[] = {"rm", "-r", waktu, NULL};`.

**Soal D & E : Membuat program killer untuk menghentikan proses dan Membuat program utama dapat di run dengan 2 mode**

    if (argc != 2 || (argv[1][1] != 'a' && argv[1][1] != 'b')) 
    {
        printf("Pilih salah satu:\nmode -a : -a\nmode -b : -b\n");
        exit(0);
    }

    int stat;
    FILE* killer;
    killer = fopen("killer.sh", "w");
    
    if (argv[1][1] == 'a') {
        fprintf(killer, "#!/bin/bash\nkill %d\nkill %d\necho 'Program Dihentikan.'\nrm \"$0\"", getpid() + 2, getpid() + 3);
    } else if (argv[1][1] == 'b') {
        fprintf(killer, "#!/bin/bash\nkill %d\necho 'Program Dihentikan.'\nrm \"$0\"", getpid() + 2);
    }

    fclose(killer);

    pid_t cid;
    cid = fork();
    if (cid < 0) exit(0);
    if (cid == 0)
    {
        char *command[] = {"chmod", "u+x", "killer.sh", NULL};
        execv("/bin/chmod", command);
    }
    while (wait(&stat) > 0);

Pada kode diatas digunakan `argc` untuk menghitung jumlah argumen dan `argv` untuk array argumen, Kemudian digunakan `if (argc != 2 || (argv[1][1] != 'a' && argv[1][1] != 'b'))` untuk menentukan mode. kemudian killer.sh dibuat dengan membuka filenya dan menggunakan `"w"` untuk menulis. isinya dimana Mode "a" akan membunuh dua proses dengan ID yang ditentukan (yaitu, getpid() + 2 dan getpid() + 3) dan menghapus dirinya sendiri. Mode "b" akan membunuh satu proses dengan ID yang ditentukan (yaitu, getpid() + 2) dan menghapus dirinya sendiri. kemudian untuk memberikan izin eksekusi killer.sh digunakan ` char *command[] = {"chmod", "u+x", "killer.sh", NULL}; execv("/bin/chmod", command);`

# Hasil Run

![compile](gambar/soal_3/compile.png)

Sebelum menjadi daemon, kita compile terlebih dahulu file c nya menggunakan `gcc lukisan.c -o lukisan`

**Mode A**

![eksekusi A](gambar/soal_3/a1.png)

File dijalankan dengan command `./lukisan -a` kemudian akan muncul sebuah file baru dengan format nama timestamp

![Killer sh A](gambar/soal_3/a2.png)

Kemudian file dihentikan dengan menggunakan perintah `./killer.sh`

![Hasil Akhir A](gambar/soal_3/a3.png)

Setelah `./killer.sh` di eksekusi, maka beginilah hasil akhirnya.

**Mode B**

![hasil setelah mode b jalan](gambar/soal_3/b1.png)

File dijalankan dengan command `./lukisan -b` kemudian akan muncul sebuah file baru dengan format nama timestamp

![Hasil setelah killer.sh](gambar/soal_3/b2.png)

Kemudian file dihentikan dengan menggunakan perintah `./killer.sh`, dan diatas merupakan tampilan awal dari program b saat `./killer.sh` di eksekusi

![Hasil Akhir B](gambar/soal_3/b3.png)

Berikut adalah hasil akhir dari program b, file akan berhenti setelah mengisi folder yang terakhir diisi dengan 15 gambar berubah menjadi zip

# Nomor 4

Choco adalah seorang ahli pertahanan siber yang tidak suka memakai ChatGPT dalam menyelesaikan masalah. Dia selalu siap melindungi data dan informasi dari ancaman dunia maya. Namun, kali ini, dia membutuhkan bantuan Anda untuk meningkatkan kinerja antivirus yang telah dia buat sebelumnya.

Bantu Choco dalam mengoptimalkan program antivirus bernama antivirus.c. Program ini seharusnya dapat memeriksa file di folder sisop_infected, dan jika file tersebut diidentifikasi sebagai virus berdasarkan ekstensinya, program harus memindahkannya ke folder quarantine. list dari format ekstensi/tipe file nya bisa didownload di Link Ini , proses mendownload tidak boleh menggunakan system()
Daftar ekstensi file yang dianggap virus tersimpan dalam file extensions.csv.
Ada kejutan di dalam file extensions.csv. Hanya 8 baris pertama yang tidak dienkripsi. Baris-baris setelahnya perlu Anda dekripsi menggunakan algoritma rot13 untuk mengetahui ekstensi virus lainnya.Setiap kali program mendeteksi file virus, catatlah informasi tersebut di virus.log. Format log harus sesuai dengan:
[nama_user][Dd-Mm-Yy:Hh-Mm-Ss] - {nama file yang terinfeksi} - {tindakan yang diambil}

Contoh:  [sisopUser][29-09-23:08-59-01] - test.locked - Moved to quarantine

*nama_user: adalah username dari user yang menambahkan file ter-infected

Dunia siber tidak pernah tidur, dan demikian juga virus. Choco memerlukan antivirus yang terus berjalan di latar belakang tanpa harus dia intervensi. Dengan menjalankan program ini sebagai Latar belakang, program akan secara otomatis memeriksa folder sisop_infected setiap detik.
Choco juga membutuhkan level-level keamanan antivrus jadi dia membuat 3 level yaitu low, medium ,hard. Argumen tersebut di pakai saat menjalankan antivirus.

    Low: Hanya me-log file yg terdeteksi
    Medium: log dan memindahkan file yang terdeteksi
    Hard: log dan menghapus file yang terdeteksi

ex: ./antivirus -p low

Kadang-kadang, Choco mungkin perlu mengganti level keamanan dari antivirus tanpa harus menghentikannya. Integrasikan kemampuan untuk mengganti level keamanan antivirus dengan mengirim sinyal ke daemon. Misalnya, menggunakan SIGUSR1 untuk mode "low", SIGUSR2 untuk "medium", dan SIGRTMIN untuk mode "hard".

Contoh:

kill -SIGUSR1 <pid_program>



Meskipun penting untuk menjalankan antivirus, ada saat-saat Choco mungkin perlu menonaktifkannya sementara. Bantu dia dengan menyediakan fitur untuk mematikan antivirus dengan cara yang aman dan efisien.
